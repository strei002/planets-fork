# -*- coding: utf-8 -*-
"""
Created on Wed Jun  5 14:04:05 2024
Edited Jun 13 @Katarina

@author: joshm
"""
#%% Libraries used
import pandas as pd
import re
import seaborn as sns
import matplotlib.pyplot as plt

#%% intake of data
dat = pd.read_csv('C:/Users/villa024/OneDrive - Wageningen University & Research/PhD/Year 1/Bioinformatics, AG and phytase/62TF851M013-Alignment-Descriptions.csv')

#%% Description cleanup
def remove_text(description):
    """ Removal of unnecessary information provided by BLAST """
    return re.sub(r'\b(complete|chromosome)\b.*|,.*', '', description, flags=re.IGNORECASE).strip()

dat['Description'] = dat['Description'].apply(remove_text)

dat.at[17, 'Description'] ='Enterococcus durans strain 4928STDY7387704'
dat.at[18, 'Description'] ='Enterococcus durans strain 4928STDY7071430'
dat.at[29, 'Description'] ='Enterococcus mundtii strain P2005'

#%% Description split for genera and species
def split_Description(Description):
    """ Splitting the information condensed in Description to have as factors in plots """
    attributes = Description.split()
    Genus_and_species = ' '.join(attributes[:2])
    Strain = ' '.join(attributes[2:])
    return Genus_and_species, Strain

dat[['Genus and species', 'Strain']] = dat['Description'].apply(lambda x: pd.Series(split_Description(x)))

#%% Generation of color palette
unique_genera = dat['Genus and species'].unique()
palette = sns.color_palette('muted', len(unique_genera))
color_map=dict(zip(unique_genera, palette))
dat['Color'] = dat['Genus and species'].map(color_map)
#%% First graph, for written documents
plt.figure(figsize = (20, 12))
ax = plt.axes()
labels = ax.get_xticklabels()
for lbl in labels:
    lbl.set_style('italic')

bars = plt.bar(dat['Description'], dat['Per. ident'], color=dat['Color'])
ax.axhline(y=100, color='red', linestyle='--')

ax.set_xlabel('')
ax.set_ylabel('Similarity to $\mathit{L.\ lactis}$ KF147 (%)',
              fontsize = 30)

plt.xticks(rotation = 80, ha = "right",
           fontsize = 16)
plt.yticks(fontsize = 16)

plt.title('Alpha-galactosidase',
          fontsize = 24)

plt.savefig('C:/Users/villa024/OneDrive - Wageningen University & Research/PhD/Year 1/Bioinformatics, AG and phytase/figures/Lal147, A-G.png',
            dpi = 300,
            bbox_inches = 'tight')

#%% Second graph, for presentations

plt.figure(figsize = (20, 18))
ax = plt.axes()

bars = plt.bar(dat['Description'],
               dat['Per. ident'],
               color = dat['Color'],
               edgecolor= 'black',
               linewidth = 3)
ax.axhline(y=100, color='red', linestyle='--')
labels = ax.get_xticklabels()
for lbl in labels:
    lbl.set_style('italic')

ax.set_xlabel('')
ax.set_ylabel('Alpha-galactosidase similarity to $\mathit{Lactococcus\ lactis}$ KF147 (%)',
              fontsize = 30)

plt.xticks(rotation = 90,
           fontsize = 24)
plt.yticks(rotation = 90,
           fontsize = 20)

plt.title('')

plt.savefig('C:/Users/villa024/OneDrive - Wageningen University & Research/PhD/Year 1/Bioinformatics, AG and phytase/figures/Lal147, A-G no title.png',
            dpi = 300,
            bbox_inches = 'tight')